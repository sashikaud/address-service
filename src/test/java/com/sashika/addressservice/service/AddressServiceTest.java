package com.sashika.addressservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link AddressService}
 *
 * Created by Sashika.Udana
 * on 18/Dec/2023
 */
public class AddressServiceTest {

    @Mock
    private NzPostApiService nzPostApiService;

    @InjectMocks
    private AddressService addressService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testSuggestAddress() {
        String inputAddress = "8 Waterloo Street";
        List<String> expectedAddresses = Arrays.asList(
                "8 Waterloo Street, Saint Kilda, Dunedin 9012",
                "8 Waterloo Street, Howick, Auckland 2014"
        );

        when(nzPostApiService.findAddresses(inputAddress)).thenReturn(expectedAddresses);

        List<String> actualAddresses = addressService.suggestAddress(inputAddress);

        assertEquals(expectedAddresses, actualAddresses);
        verify(nzPostApiService, times(1)).findAddresses(inputAddress);
    }
}
