package com.sashika.addressservice.service.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link JsonStringHandler}
 *
 * Created by Sashika.Udana
 * on 18/Dec/2023
 */
public class JsonStringHandlerTest {

    @Mock
    private ObjectMapper objectMapper;

    private JsonStringHandler jsonStringHandler;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        jsonStringHandler = new JsonStringHandler(objectMapper);
    }

    @Test
    void testGetValue() throws JsonProcessingException {
        String jsonString = "{\"key1\": \"value1\"}";
        when(objectMapper.readTree(jsonString)).thenReturn(new ObjectMapper().readTree(jsonString));

        String value = jsonStringHandler.getValue(jsonString, "key1");

        assertEquals("value1", value);
    }

    @Test
    void testGetValueJsonProcessingException() throws JsonProcessingException {
        String jsonString = "{\"key1\": \"value1\"}";
        when(objectMapper.readTree(jsonString)).thenThrow(JsonProcessingException.class);

        assertThrows(RuntimeException.class, () -> jsonStringHandler.getValue(jsonString, "key1"));
    }

    @Test
    void testGetAttributesFromArray() throws JsonProcessingException {
        String jsonString = "{\"array\": [{\"attribute\": \"valueA\"}, {\"attribute\": \"valueB\"}]}";
        JsonNode jsonNode = new ObjectMapper().readTree(jsonString);
        when(objectMapper.readTree(jsonString)).thenReturn(jsonNode);

        List<String> attributeValues = jsonStringHandler.getAttributesFromArray(jsonString, "array", "attribute");

        assertEquals(2, attributeValues.size());
        assertTrue(attributeValues.contains("valueA"));
        assertTrue(attributeValues.contains("valueB"));
    }

    @Test
    void testGetAttributesFromArrayJsonProcessingException() throws JsonProcessingException {
        String jsonString = "{\"array\": [{\"attribute\": \"valueA\"}, {\"attribute\": \"valueB\"}]}";
        when(objectMapper.readTree(jsonString)).thenThrow(JsonProcessingException.class);

        assertThrows(RuntimeException.class, () -> jsonStringHandler.getAttributesFromArray(jsonString, "array", "attribute"));
    }
}
