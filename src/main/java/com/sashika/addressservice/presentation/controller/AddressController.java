package com.sashika.addressservice.presentation.controller;

import com.sashika.addressservice.service.AddressService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller for handling address-related requests.
 * Created by Sashika.Udana
 * on 18/Dec/2023
 */
@RestController
@RequestMapping
public class AddressController {

    private final AddressService addressService;

    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    /**
     * Handles POST requests to suggest addresses for user's address.
     *
     * @param userAddress The user's address to suggest addresses.
     * @return ResponseEntity with the results of the similar addresses.
     */
    @GetMapping("/suggestAddress")
    public ResponseEntity<List<String>> suggestAddress(@RequestParam String userAddress) {
        List<String> similarAddresses = addressService.suggestAddress(userAddress);
        return ResponseEntity.ok(similarAddresses);
    }
}
