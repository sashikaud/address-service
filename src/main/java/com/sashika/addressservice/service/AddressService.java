package com.sashika.addressservice.service;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service class to handle business logic for address-related operations.
 * <p>
 * Created by Sashika.Udana
 * on 18/Dec/2023
 */
@Service
public class AddressService {

    private final NzPostApiService nzPostApiService;

    public AddressService(NzPostApiService nzPostApiService) {
        this.nzPostApiService = nzPostApiService;
    }

    /**
     * Suggests addresses for given address
     *
     * @param address The address for which suggestions are return
     * @return List of addresses similar to given address
     */
    public List<String> suggestAddress(String address) {
        return nzPostApiService.findAddresses(address);
    }
}
