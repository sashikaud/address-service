package com.sashika.addressservice.service;

import com.sashika.addressservice.service.util.JsonStringHandler;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.time.Instant;
import java.util.List;

/**
 * Created by Sashika.Udana
 * on 18/Dec/2023
 */
@Service
public class NzPostApiService {

    @Value("${nz.post.api.host}")
    private String nzPostApiHost;

    @Value("${nz.post.api.address.finder}")
    private String addressFinderEndpoint;

    @Value("${nz.post.api.address.finder.queryParam}")
    private String addressFinderQueryParam;

    @Value("${nz.post.client.id}")
    private String clientId;

    @Value("${nz.post.client.secret}")
    private String clientSecret;

    @Value("${nz.post.token.endpoint}")
    private String tokenEndpoint;

    @Value("${nz.post.token.expiration.seconds}")
    private long tokenExpirationSeconds;

    private final RestTemplate restTemplate;
    private final JsonStringHandler jsonStringHandler;
    private String accessToken;
    private Instant tokenExpiration;

    public NzPostApiService(RestTemplate restTemplate, JsonStringHandler jsonStringHandler) {
        this.restTemplate = restTemplate;
        this.jsonStringHandler = jsonStringHandler;
    }

    /**
     * Finds similar addresses based on the given address
     *
     * @param address The address to find similar ones
     * @return List of addresses similar to given address
     */
    public List<String> findAddresses(String address) {
        ensureAccessTokenValidity();

        URI apiEndpoint = UriComponentsBuilder
                .fromUriString(nzPostApiHost)
                .pathSegment(addressFinderEndpoint)
                .queryParam(addressFinderQueryParam, address)
                .build()
                .toUri();

        HttpHeaders headers = new HttpHeaders();
        headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
        headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);

        ResponseEntity<String> response = restTemplate.exchange(
                apiEndpoint,
                HttpMethod.GET,
                new HttpEntity<>(headers),
                String.class);

        return jsonStringHandler.getAttributesFromArray(response.getBody(), "addresses", "FullAddress");
    }

    private void ensureAccessTokenValidity() {
        if (accessToken == null || Instant.now().isAfter(tokenExpiration)) {
            accessToken = getAccessToken();
            tokenExpiration = Instant.now().plusSeconds(tokenExpirationSeconds);
        }
    }

    private String getAccessToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> requestBodyMap = new LinkedMultiValueMap<>();
        requestBodyMap.add("grant_type", "client_credentials");
        requestBodyMap.add("client_id", clientId);
        requestBodyMap.add("client_secret", clientSecret);

        RequestEntity<MultiValueMap<String, String>> request = RequestEntity
                .post(URI.create(tokenEndpoint))
                .headers(headers)
                .body(requestBodyMap);

        ResponseEntity<String> response = restTemplate.exchange(request, String.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            return jsonStringHandler.getValue(response.getBody(), "access_token");
        } else {
            throw new RuntimeException("Failed to obtain access token. HTTP Status: " + response.getStatusCode());
        }
    }
}
