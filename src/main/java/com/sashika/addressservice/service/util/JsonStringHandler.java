package com.sashika.addressservice.service.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for handling JSON strings
 * Created by Sashika.Udana
 * on 18/Dec/2023
 */
@Component
public class JsonStringHandler {

    private final ObjectMapper objectMapper;

    public JsonStringHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }


    /**
     * Retrieves the value associated with the given key from the JSON string.
     *
     * @param jsonString the JSON string to extract the value from.
     * @param key         the key to search for in the JSON string.
     * @return the value associated with the key, or null if the key is not found.
     * @throws RuntimeException if there is an error processing the JSON string.
     */
    public String getValue(String jsonString, String key) {
        try {
            return objectMapper.readTree(jsonString).get(key).asText();
        } catch (JsonProcessingException jsonProcessingException) {
            throw new RuntimeException(jsonProcessingException.getMessage());
        }
    }

    /**
     * Retrieves values of a specific attribute from an array in the JSON string.
     *
     * @param jsonString  the JSON string containing the array.
     * @param arrayName   the name of the array to extract values from.
     * @param attribute   the attribute to retrieve from each element of the array.
     * @return a list of values for the specified attribute from the array.
     * @throws RuntimeException if there is an error processing the JSON string.
     */
    public List<String> getAttributesFromArray(String jsonString, String arrayName, String attribute) {
        List<String> values = new ArrayList<>();

        try {
            JsonNode jsonNode = objectMapper.readTree(jsonString);
            JsonNode nodes = jsonNode.get(arrayName);

            for (JsonNode node : nodes) {
                values.add(node.get(attribute).asText());
            }
        } catch (JsonProcessingException jsonProcessingException) {
            throw new RuntimeException(jsonProcessingException.getMessage());
        }

        return values;
    }

}
